# Pyramao Rules

The below rules go on one sheet, on the table, face down, in front of the Chair.

---

"delay of game"

Any player that doesn't take their turn promptly, within about 5 seconds of the last action, may be punished for "delay of game".

"speaking out of turn"

Any player engaging in speech which is not specifically required or allowed may be punished for "speaking out of turn".

"point of order"

Any player may, at the beginning of any turn, stand and say "point of order". All players must promptly stand, and the game is paused. When it is not possible for a player to stand, they may move away from the table. When that's not possible, they may pull their hands away from the table. incapable of standing may move away from the table. may raise their No player may remain at the table or touch any game element during a point of order. A "point of order" may be called to: answer the door, get refreshments, discuss rules, or whatever else is disallowed by the normal rules. A point of order is ended when all players return to the table (standing away from it), and the person that called the point of order says "Thank you". At that point, all players return to the table and the game resumes.

"winning"

As briefly mentioned, when a player is unable to play a pyramid as a result of having previously played them all, that player wins the round.

"Lemme see that sheet"

When a player is punished, they may say, "lemme see that sheet". If the sheet supports the punishment, the player that punished them may punish them again. If it does not, their punishment is undone, and they may punish the player that punished them inappropriately.

"failure to uphold the law"

Any player (other than the Chair) may be punished for failing to punish a person that breaks a law that is assigned to them.