# Pyramao

a slightly sadistic game for democrats and dictators

The name of this game is Pyramao or LMAO, pronounced peer'-uh-mao or ell'-mao, where mao rhymes with "Ow!"

The goal of Pyramao is to gather and retain political power, to have fun, to avoid punishment, or to heap punishment on other(s). You choose.

By the time you are reading this, for the first time, it is probably time for the player seated to the left of the parliamentarian, president, dictator, head honcho, MFWIC, chairperson, dear leader, boss, henceforth referred to as the Chair, to take his or her turn, and if s/he doesn't get about it, s/he may be punished.

How do we determine who is the Chair (in order to determine who is to to the left of the Chair)? The player who first said the word "Mao" (or "Pyramao"), except in an unambiguous statement of repudiation, like, "Mao? I hate that game!" is the Chair.

But what should s/he *do*? That depends on which rules are in play. If there are no pyramids in front of him or her, and the Chair has not declared that everyone wins, and no other player has declared that *they* win (and gotten away with it), it's probably safe to reach into the draw pile and draw a pyramid.

But don't draw a pyramid that can't be drawn alone. And don't draw a pyramid if the "load up" phase of the game is over. Unless you can't play a pyramid, then you have to draw. Unless it's not your turn, then drawing or playing a pyramid or saying anything other than "point of order" will get you punished.

Got it?

---

We guess that this game work best with 5-7 players. We further guess that the game will play best with about the same number of colors/stashes as players. But, we'll know better after we playtest a bit.

---

Phase 0: Assembly

During this phase, the Chair will suggest Pyramao. Others will foolishly agree, and the Chair will set out the rules and laws face down. The Chair may assign laws to players for codification and enforcement. Ls not assigned to players are assigned to the Chair. If a rule is not assigned to a player or to the Chair, it is assigned to all the players equally. All players, including the Chair, are bound by the rules on the table.

Only the Chair may sit at the table or touch any game element during this time. Players violating this rule may be punished by the Chair. Yes, even before the game starts.

The Chair will set out the Rules and active Laws, face down. Pyramids in a draw pile in the center of the table, with one designated as the "last played pyramid". It does not have to *actually* be the last played pyramid from a previous game or round.

Phase 1: Drawing up

The Chair calls the other players to the table. All players take a seat at the table. The Chair may make a little speech at this point, telling the players how it's going to be, and thanking them in advance for their unconditional loyalty.

Starting with the player to the left of the Chair and proceeding clockwise, players take turns drawing from the pile. During this phase, players should place pyramids on rules that have been assigned to them that say, "place a pyramid on the back of this rule".

Phase 2: The Play

The active player must **play** a pyramid by putting one of their pyramids back in the draw pile which matches the size or color of the last pyramid played. If you have pyramids, but none that match, you must **draw** one, instead. If you have no pyramids (presumably because you've played them all), you win!

Any player may **punish** any player who has broken a rule by giving them a pyramid from the draw pile and telling them their infraction.

Phase 3: Judgement Day

After a round of pyramao, all players (including the Chair) will secretly vote on how benevolent a dictator s/he is.

* small = "I think (s)he's nice!"
* medium/none = I er... abstain.
* large = "Down with the Emperor!"

Count smalls vs larges. If and only if larges win, large votes must now reveal whether they personally are willing to lead the mutinous mob. Each large vote shows whether they were hiding a small.

* large hiding a small = "I will be king!"
* large with no small = "We don't have a lord."

[LICENSE](LICENSE.md)
[CREDITS](CREDITS.md)
[RULES](RULES.md)
[LAWS](LAWS.md)
[SOURCE](https://gitlab.com/LPGD/pyramao/)
