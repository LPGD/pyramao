# Pyramao Laws

Print or write these rules on index cards, playing cards, or sheets of paper, one rule per sheet.

The Chair will decide which rules will be used in each round of the game. One or more rules may be assigned to players for enforcement. All rules that are not assigned to a player are assigned to the Chair.

---

"Mao"

A player playing their last pyramid, must play the pyramid, say "Mao", and stand up, in any order, but in quick succession.

If a player does not say "Mao", the player may be punished for "lying".

If a player does not stand up, the player may be punished for "cheating".

A player caught lying *and* cheating, may additionally be punished for "grand theft auto". It's reasonable to assume that if they'll lie and cheat, they *would* download a car.

---

"Long live the $COLOR queen!"

Place a large pyramid on the back of this rule card.

When identical pyramids are played, all players must enthusiastically shout "Long live the $COLOR queen!"

Any player that fails to do so may be punished for "failing to honor the queen"

---

"first right of refusal"

Any player may punish any player for a violation of any rule.

This rule replaces "uphold the law". The player to whom a rule is assigned *may* punish rule-breakers, but if s/he does not do so promptly, any other player may.

Players that don't wait at least three seconds before punishing, may be punished by the rule-holder for failure to yield "first right of refusal".

Example: If a rule is assigned to Alice, and Bob breaks the rule, Charli may punish Bob after 3 seconds. If Charli punishes Bob faster than that, Alice may punish Charli for failure to yield "first right of refusal". If Charli *did* wait before punishing Bob, and Alice punishes Charli anyway, Charli may punish Alice for "delay of game".

---

"ragnarok"

Place a large black pyramid on the back of this rule card and say "three".

Whenever a pyramid matching the pyramid on the back of this card is played, swap the pyramid on the back of this card with the next lower size (from the draw pile, your own pyramids, or any other player's pyramids) and say the new pip size out loud: "two", then "one", then remove the pyramid entirely, say "zero", toss this card to the Chair as stylishly as you can, and then hand out all the black pyramids from the draw pile, starting to the left of the player that played the pyramid that triggered it, saying "a little ragnarok for you" "and a little ragnarok for you" as you go. Do not skip yourself; you need some ragnarok, too.

---

"$COLOR is Wild"

Place a pyramid on the back of this card, of a color not yet being used for another rule.

Pyramids of this color may be played after any color, and the active player must say a color-name. The next player plays as if the last-played pyramid were that color.

---

"$COLOR is Draw Two"

Place a pyramid on the back of this card, of a color not yet being used for another rule.

When this color is played, the subsequent player must draw two pyramids and may not play one.

---

"$COLOR is Reverse"

Place a pyramid on the back of this card, of a color not yet being used for another rule.

Whenever this color is played, the direction of play is reversed.

---

"We all go down together!"

Place a pyramid on the back of this card, of a color not yet being used for another rule.

Whenever this color is played by the active player, all other players may play a pyramid of this color, while singing "We all go down together!"
